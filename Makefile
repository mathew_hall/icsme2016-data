OPEN := open

.PHONY: summary
summary: 
	make -C submission

launch-jupyter-native:
	jupyter notebook data-analysis.ipynb

launch-jupyter-docker:
	docker run -p8888:8888 --rm -v /$$(pwd)/data-analysis.ipynb:/nb/data-analysis.ipynb jupyter/r-notebook start-notebook.sh /nb/data-analysis.ipynb
	$(OPEN) http://localhost:8888/notebooks/data-analysis.ipynb