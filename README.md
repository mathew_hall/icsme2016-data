# ICSME 2016 Scripts & Analysis

This repository contains the scripts used to run our experiments and a Jupyter notebook that interactively describes our analysis.

Please see the description in the `submission` folder for instructions on how to use the supplied Makefile and notebooks.

# Running Jupyter in Docker

We have tested the Docker container on Mac OSX. When running in Docker, the repository needs to be located [within your home directory](https://github.com/docker/machine/issues/1826).

We have not tested the Docker container on Windows, but provided it is run from your home directory it should work.

# Licence

This repository is licensed under the Apache Software 2.0 License. Please see the LICENSE file for the full text of the licence.