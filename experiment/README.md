# MINT Experiments

This release runs the experiments from our ICSME2016 paper. It handles the process of downloading and building MINT and its dependencies and runs it sequentially on the subjects from the paper.

## Usage

### Generate results

    make

### Generate an example result

This target builds and runs MINT once on liftDoors2. It saves to `results/liftDoors2/liftDoors2.0.log`.

    make example-data

### Only Generate MINT

This target stops when `EFSMTool.jar` has been built and placed in the current directory.

    make EFSMTool.jar

### Run experiments

This target runs the whole workflow. On multicore machines using the `-j` option may speed up the process (e.g. `make -j 4 all-experiments`).

    make all-experiments

## Requirements

1. Maven
2. Mercurial
3. Java JDK 1.6
4. Make

## What it does

The Makefile describes how to fetch MINT from source and build it (including downloading and installing Major). It places the built JAR in `EFSMTool.jar`.

Once the JAR is available, the `run_mint.sh` script can be used to generate results for a tracefile. It takes three arguments:

    ./run_mint.sh TraceFiles/<trace> <output filename> <random seed>

The Makefile supplied performs 30 runs of MINT on `liftDoors2` and `cruiseTrace`.

Results are dumped into the `results` directory. These files are compatible with the data processing Jupyter notebook in the root of the repository.